# Naoqi Robot Embodiment

[API Docs](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos_pkg_naoqi_robot_embodiment)

Embodiment module for naoqi robots (pepper, nao) that uses the naoqi SDK.

# Install

Similar to the QiBullet Sim, you need another conda environment with python 2.7.

```bash
conda create -n py27 python=2.7
conda activate py27
```

[Here](http://doc.aldebaran.com/2-5/dev/python/install_guide.html) you can check how to install the naoqi SDK (for the communication with the robot).

For Linux: Just download the Python 2.7 Naoqi 2.8.6 Version from [here](https://www.aldebaran.com/en/support/nao-6/downloads-softwares). And put the extracted content somewhere. Replace the `naoqi_sdk_path` config variable with your path to the sdk AND the path inside the extracted folders to the site-packages (see the example string).

For further examples how to use the naoqi SDK see http://doc.aldebaran.com/2-8/dev/python/examples.html#python-examples and http://doc.aldebaran.com/2-8/naoqi/index.html (API Docs).

`pip install -e .`

