"""

This is the documentation of the aaambos package NaoQi Robot Embodiment.
It contains ofaaambos modules, owm run and/or arch configs, 

# About the package

# Background / Literature

# Usage / Examples

## Install

Similar to the QiBullet Sim, you need another conda environment with python 2.7.

```bash
conda create -n py27 python=2.7
```

[Here](http://doc.aldebaran.com/2-5/dev/python/install_guide.html) you can check how to install the naoqi SDK (for the communication with the robot).

For Linux: Just download the Python 2.7 Naoqi 2.8.6 Version from [here](https://www.aldebaran.com/en/support/nao-6/downloads-softwares). And put the extracted content somewhere. Replace the `naoqi_sdk_path` config variable with your path to the sdk AND the path inside the extracted folders to the site-packages (see the example string).

For further examples how to use the naoqi SDK see http://doc.aldebaran.com/2-8/dev/python/examples.html#python-examples and http://doc.aldebaran.com/2-8/naoqi/index.html (API Docs).

`pip install -e .`

## Usage

The module config of the naoqi backend needs configuration:
```yaml
  naoqi_backend:
    module_info: !name:naoqi_robot_embodiment.modules.naoqi_backend.NaoQiBackend
    conda_script: "~/miniconda3/etc/profile.d/conda.sh"
    conda_env_name: "py27"
    robot: "pepper"
    robot_ip: 127.0.0.2
    computer_ip_plus_open_port: 127.0.0.1:12345
    naoqi_sdk_path: "~/miniconda3/envs/py27/lib/python2.7/site-packages/naoqi/lib/python2.7/site-packages"
```

First you need to specify if you use the `pepper`or `nao` robot. You need the ip address of the robot (clicking on the button on the robot on the chest). Your computer and the robot need to be in the same network. For receiving events from the robot you need to specify the ip of your computer (`ip address` in command line) and open a random port for example 33729 (`sudo ufw allow 33729`). Specify everything in the config. You can overwrite it easily in the `run_config`. 

```
module_options:
  naoqi_backend:
    robot_ip: "192.123.123.11"  
    robot: "nao"
    computer_ip_plus_open_port: "192.321.321.21:33729"   # check your ip with "ip address" and open port with "sudo ufw allow 33729"
```

Check the correct  `naoqi_sdk_path` and your `conda_script` location.

# Citation


"""