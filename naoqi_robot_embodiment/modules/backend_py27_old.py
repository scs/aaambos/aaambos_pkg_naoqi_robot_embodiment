#!/usr/bin/env python2.7

import argparse
import sys
import json
import time
import traceback

from threading import Thread

import qi
from naoqi import ALProxy


class NaoQiBackend(object):

    def __init__(self, args, session):
        self.session = session
        self.tts = ALProxy("ALTextToSpeech", args.ip, args.port)
        self.tts.say("Hello, world!")

        self.animation_player = session.service("ALAnimationPlayer")

        self.systeminfo = ALProxy("ALSystem", args.ip, args.port)
        self.tts.say("I am running on Version" + str(self.systeminfo.systemVersion()))

        self.input_handler_thread = Thread(target=self.process_stdin)
        self.input_handler_thread.start()

    def process_stdin(self):
        line_to_complete = ""
        threads = []
        while True:
            line = raw_input()
            try:
                line_to_complete += line
                topic_msg = json.loads(line_to_complete)
                line_to_complete = ""
                assert "topic" in topic_msg and "msg" in topic_msg, "require topic and msg as keys in input string"
                self.callback(topic_msg["topic"], topic_msg["msg"], threads)
            except ValueError:
                pass
            except (KeyboardInterrupt, SystemExit):
                sys.exit(0)
            except:
                sys.stderr.write(traceback.format_exc())

            if threads and not threads[0].is_alive():
                threads.pop(0)

    def callback(self, topic, msg, threads):
        if topic == "runAnimation":
            self.do_async(self.animation_player.run, threads, msg["animation_path"])
        if topic == "runTagAnimation":
            self.do_async(self.animation_player.runTag, threads, msg["tag"])
        if topic == "say":
            self.do_async(self.tts.say, threads, str(msg["text"]))


    def do_async(self, func, thread_list, *args, **kwargs):
        thread = Thread(target=func, args=args, kwargs=kwargs)
        thread.start()
        thread_list.append(thread)

    def send_msg(self, topic, msg):
        print str(json.dumps({"topic": topic, "msg": msg})) + "\n"

    def run(self):
        steps = 0
        while True:
            if steps == 1:
                self.send_msg("info", {"i": "started"})

            steps += 1
            time.sleep(0.5)



def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--pattern", type=str, help="the pattern to print")
    parser.add_argument("--robot", type=str, default="pepper", help="Which robot to control, ether 'nao' or 'pepper'")
    parser.add_argument("--ip", type=str, default="127.0.0.1",
                        help="Robot IP address. On robot or Local Naoqi: use '127.0.0.1'.")
    parser.add_argument("--port", type=int, default=9559, help="Naoqi port number")

    args = parser.parse_args()
    session = qi.Session()
    try:
        session.connect("tcp://" + args.ip + ":" + str(args.port))
    except RuntimeError:
        print("Can't connect to Naoqi at ip \"" + args.ip + "\" on port " + str(args.port) + ".\n"
                                                                                             "Please check your script arguments. Run with -h option for help.")
        sys.exit(1)
    backend = NaoQiBackend(args, session)
    backend.run()

if __name__ == "__main__":
    main()

