from __future__ import annotations

import asyncio
import json
import os
import random
import time
from pathlib import Path
from subprocess import Popen, PIPE
from typing import Type, Callable

from aaambos.core.supervision.run_time_manager import ControlMsg
from aaambos.std.extensions.pipe_subprocess.pipe_subprocess import PipeSubprocessExtension, \
    PipeSubprocessExtensionFeature
from attrs import define

from aaambos.core.configuration.module_config import ModuleConfig
from aaambos.core.module.base import Module, ModuleInfo
from aaambos.core.module.feature import FeatureNecessity, Feature, SimpleFeatureNecessity
from simple_flexdiam.modules.core.definitions import SpeechGenerationControlComFeatureOut, \
    SpeechGenerationStatusComFeatureIn, SpeechGenerationStatusComFeatureOut, SpeechGenerationControlComFeatureIn, \
    SPEECH_GENERATION_CONTROL, SpeechGenerationControl, Controls, SPEECH_GENERATION_STATUS, SpeechGenerationStatus, \
    Status

from naoqi_robot_embodiment.modules.robot_jumpstarter_python3.python3.stk.events import EventHelper
from naoqi_robot_embodiment.modules.robot_jumpstarter_python3.python3.stk.services import ServiceCache
from naoqi_robot_embodiment.modules.robot_jumpstarter_python3.python3.stk.python27bridge import Python27Bridge
from naoqi_robot_embodiment.naoqi_names.animation_paths import naoqi_animation_paths
from naoqi_robot_embodiment.naoqi_names.animation_tags import naoqi_tags
from naoqi_robot_embodiment.naoqi_names.event_names import NaoQiNames


class NaoQiBackend(Module, ModuleInfo):
    config: NaoQiBackendConfig

    def __init__(self, config: ModuleConfig, com, log, ext, *args,
                 **kwargs):
        super().__init__(config, com, log, ext, *args, **kwargs)
        self.subprocess: Popen | None = None
        self.steps = 0
        self.python27bridge = None
        self.events = None
        self.s = None

        self.animated_speeches = []

    @classmethod
    def provides_features(cls, config: ModuleConfig = ...) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        return {
            SpeechGenerationControlComFeatureIn.name: (
            SpeechGenerationControlComFeatureIn, SimpleFeatureNecessity.Required),
            SpeechGenerationStatusComFeatureOut.name: (
            SpeechGenerationStatusComFeatureOut, SimpleFeatureNecessity.Required),

        }

    @classmethod
    def requires_features(cls, config: ModuleConfig = ...) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        return {
            SpeechGenerationControlComFeatureOut.name: (
            SpeechGenerationControlComFeatureOut, SimpleFeatureNecessity.Required),
            SpeechGenerationStatusComFeatureIn.name: (
            SpeechGenerationStatusComFeatureIn, SimpleFeatureNecessity.Required),
        }

    @classmethod
    def get_module_config_class(cls) -> Type[ModuleConfig]:
        return NaoQiBackendConfig

    async def initialize(self):
        await self.com.register_callback_for_promise(self.prm[SPEECH_GENERATION_CONTROL], self.handle_tts_control)

        self.subprocess = Popen(["sh", os.path.expanduser(self.config.shell_script),
                os.path.expanduser(self.config.conda_script),
                self.config.conda_env_name,
                os.path.expanduser(self.config.naoqi_sdk_path),
                self.config.robot_ip,
                self.config.computer_ip_plus_open_port])
        await asyncio.sleep(3)
        self.python27bridge = Python27Bridge()
        self.events = EventHelper(self.python27bridge)
        self.s = ServiceCache(self.python27bridge)
        self.connect_event(NaoQiNames.ALTextToSpeech_TextDone, self.animated_speech_end)
        self.s.ALTextToSpeech.setLanguage("German")
        # x, y = self.subprocess.communicate()
        # if self.subprocess.returncode:
        #     while x is None:
        #         self.log.info(x)
        #         self.log.info(y)
        #         x, y = self.subprocess.communicate()
        #     self.log.info(list(self.subprocess.stdout.readlines()))
        self.s.ALTextToSpeech.say("Piep!")

    async def step(self):
        ...

    def terminate(self, control_msg: ControlMsg = None) -> int:
        exit_code = super().terminate()
        self.log.info("Try kill python 2")
        self.subprocess.kill()
        self.subprocess.kill()
        self.log.info("Killed python 2")
        return exit_code

    def animated_speech_end(self, args):
        self.log.info("Animated speech ended..")

        if args:
            self.log.info(args)

        if self.animated_speeches:
            sp = self.animated_speeches.pop(0)
            loop = asyncio.new_event_loop()
            loop.run_until_complete(self.com.send(self.tpc[SPEECH_GENERATION_STATUS], SpeechGenerationStatus(id=sp, status=Status.finished)))
            loop.close()

    async def handle_tts_control(self, topic, speech_control: SpeechGenerationControl):
        if speech_control.control == Controls.START:
            self.log.info(f"Verbalize {speech_control.control} {speech_control.text}")
            self.s.ALTextToSpeech.say(speech_control.text)
            self.animated_speeches.append(speech_control.id)
            #await asyncio.sleep(2)
            #await self.com.send(self.tpc[SPEECH_GENERATION_STATUS], SpeechGenerationStatus(id=speech_control.id, status=Status.finished))

    def connect_event(self, event_name: str, callback: Callable[[str], None]):
        if self.config.robot.lower() == "pepper":
            self.log.info(f"connect pepper event {event_name}")
            self.events.connect(f"_{event_name}", callback)
        else:
            self.log.info(f"connect nao event {event_name}")
            self.events.connect(event_name, callback)


@define(kw_only=True)
class NaoQiBackendConfig(ModuleConfig):
    robot_ip: str
    computer_ip_plus_open_port: str
    conda_script: str
    naoqi_sdk_path: str
    module_path: str = "naoqi_robot_embodiment.modules.naoqi_backend"
    module_info: Type[ModuleInfo] = NaoQiBackend
    restart_after_failure: bool = False
    expected_start_up_time: float | int = 0
    robot: str = "pepper"
    shell_script: str = Path(os.path.dirname(os.path.realpath(__file__)), "run_sim_27.sh")
    conda_env_name: str = "py27"


def provide_module():
    return NaoQiBackend
