from __future__ import annotations

import json
import os
import random
import time
from pathlib import Path
from typing import Type

from aaambos.core.supervision.run_time_manager import ControlMsg
from aaambos.std.extensions.pipe_subprocess.pipe_subprocess import PipeSubprocessExtension, \
    PipeSubprocessExtensionFeature
from attrs import define

from aaambos.core.configuration.module_config import ModuleConfig
from aaambos.core.module.base import Module, ModuleInfo
from aaambos.core.module.feature import FeatureNecessity, Feature, SimpleFeatureNecessity

from naoqi_robot_embodiment.naoqi_names.animation_paths import naoqi_animation_paths
from naoqi_robot_embodiment.naoqi_names.animation_tags import naoqi_tags


class NaoQiBackend(Module, ModuleInfo):
    config: NaoQiBackendConfig

    def __init__(self, config: ModuleConfig, com, log, ext, subprocess: PipeSubprocessExtension, *args,
                 **kwargs):
        super().__init__(config, com, log, ext, *args, **kwargs)
        self.subprocess = subprocess
        self.steps = 0

    @classmethod
    def provides_features(cls, config: ModuleConfig = ...) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        return {}

    @classmethod
    def requires_features(cls, config: ModuleConfig = ...) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        return {
            PipeSubprocessExtensionFeature.name: (PipeSubprocessExtensionFeature, SimpleFeatureNecessity.Required)
        }

    @classmethod
    def get_module_config_class(cls) -> Type[ModuleConfig]:
        return NaoQiBackendConfig

    async def initialize(self):
        await self.subprocess.start_subprocess(
            [
                "sh",
                os.path.expanduser(self.config.shell_script),
                os.path.expanduser(self.config.conda_script),
                self.config.conda_env_name,
                os.path.expanduser(self.config.naoqi_sdk_path),
                self.config.robot,
                self.config.robot_ip
            ], callback=self.sim_callback, converter=self.convert_line)

    def convert_line(self, line):
        line = json.loads(line)
        return line["topic"], line["msg"]

    async def sim_callback(self, line):
        topic, msg = line
        self.log.error("->", topic, msg)

    async def step(self):
        self.steps += 1
        if self.steps == 3:
            tag = random.choice(naoqi_tags[self.config.robot])
            self.log.info(f"Ich animiere {tag}")
            await self.subprocess.msg_subprocess({"topic": "runTagAnimation", "msg": {"tag": tag}})
            await self.subprocess.msg_subprocess({"topic": "say", "msg": {"text": f"Data Scientists sind tolle Menschen!"}})

        if self.steps == 15:
            animation_path = random.choice(naoqi_animation_paths[self.config.robot])
            self.log.info(f"Ich animiere {animation_path!r}")
            await self.subprocess.msg_subprocess({"topic": "runAnimation", "msg": {"animation_path": animation_path}})
            await self.subprocess.msg_subprocess({"topic": "say", "msg": {"text": f"Animation {animation_path}"}})

    def terminate(self, control_msg: ControlMsg = None) -> int:
        exit_code = super().terminate()
        self.subprocess.terminate_sub_process()
        return exit_code


@define(kw_only=True)
class NaoQiBackendConfig(ModuleConfig):
    robot_ip: str
    conda_script: str
    naoqi_sdk_path: str
    module_path: str = "naoqi_robot_embodiment.modules.naoqi_backend"
    module_info: Type[ModuleInfo] = NaoQiBackend
    restart_after_failure: bool = True
    expected_start_up_time: float | int = 0
    robot: str = "pepper"
    shell_script: str = Path(os.path.dirname(os.path.realpath(__file__)), "run_sim_27.sh")
    conda_env_name: str = "py27"


def provide_module():
    return NaoQiBackend
