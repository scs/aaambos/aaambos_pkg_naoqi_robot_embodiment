#!/bin/sh

# bash script that activates a conda environment and runs the simulator_py39.py
#
# first argument: path to conda.sh, e.g., ~/miniconda3/etc/profile.d/conda.sh
# second argument: the name of the conda environment

. $1
SCRIPTPATH=$(dirname "$0")
conda activate $2
export PYTHONPATH=${PYTHONPATH}:$3
python $SCRIPTPATH/robot_jumpstarter_python3/python27/naoqiconnection.py --qi-url=$4  --listen=$5
