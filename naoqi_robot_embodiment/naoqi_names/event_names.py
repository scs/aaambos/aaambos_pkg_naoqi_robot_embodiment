from enum import Enum

event_names = {
    "ALAnimatedSpeech/EndOfAnimatedSpeech",
    "ALAudioSourceLocalization/SoundLocated",
    "ALAudioSourceLocalization/SoundsLocated",
    "ALBasicAwareness/HumanLost",
    "ALBasicAwareness/HumanTracked",
    "ALBasicAwareness/StimulusDetected",
    "ALBattery/ConnectedToChargingStation",
    "ALBehaviorManager/BehaviorAdded",
    "ALBehaviorManager/BehaviorRemoved",
    "ALBehaviorManager/BehaviorUpdated",
    "ALBehaviorManager/BehaviorsAdded",
    "ALChestButton/DoubleClickOccurred",
    "ALChestButton/SimpleClickOccurred",
    "ALChestButton/TripleClickOccurred",
    "ALDiagnosis/RobotIsReady",
    "ALLocalization/FullScanBegin",
    "ALLocalization/FullScanInsufficient",
    "ALLocalization/FullScanSuccess",
    "ALLocalization/GoToBegin",
    "ALLocalization/GoToFailed",
    "ALLocalization/GoToLost",
    "ALLocalization/GoToNextMove",
    "ALLocalization/GoToSuccess",
    "ALLocalization/HalfScanBegin",
    "ALLocalization/HalfScanInsufficient",
    "ALLocalization/HalfScanSuccess",
    "ALLocalization/LocalizeBegin",
    "ALLocalization/LocalizeDirectionBegin",
    "ALLocalization/LocalizeDirectionLost",
    "ALLocalization/LocalizeDirectionSuccess",
    "ALLocalization/LocalizeLost",
    "ALLocalization/LocalizeSuccess",
    "ALLocalization/OdometryBegin",
    "ALLocalization/OdometryInsufficient",
    "ALLocalization/StartingComputation",
    "ALLocalization/StoppingComputation",
    "ALLocalization/UTurnEnd",
    "ALMemory/KeyAdded",
    "ALMemory/KeyRemoved",
    "ALMemory/KeyTypeChanged",
    "ALMotion/MoveFailed",
    "ALMotion/Protection/DisabledDevicesChanged",
    "ALMotion/RobotIsFalling",
    "ALMotion/RobotIsStand",
    "ALMotion/RobotPushed",
    "ALMotion/Safety/ChainVelocityClipped",
    "ALMotion/Safety/PushRecovery",
    "ALMotion/Stiffness/wakeUpFinished",
    "ALMotion/Stiffness/wakeUpStarted",
    "ALRecharge/CloseToChargingStation",
    "ALRecharge/ConnectedToChargingStation",
    "ALRecharge/DockingBackwardStarted",
    "ALRecharge/DockingFailed",
    "ALRecharge/DockingSuccess",
    "ALRecharge/DockingUTurnStarted",
    "ALRecharge/LeaveFailed",
    "ALRecharge/LeaveSuccess",
    "ALRecharge/MoveFailed",
    "ALRecharge/SearchStopped",
    "ALRecharge/StationDetected",
    "ALRecharge/StationNotFound",
    "ALRecharge/StatusChanged",
    "ALSoundLocalization/SoundLocated",
    "ALSoundLocalization/SoundsLocated",
    "ALSpeechRecognition/ActiveListening",
    "ALSpeechRecognition/IsRunning",
    "ALSpeechRecognition/Status",
    "ALStore/SystemImageDownloaded",
    "ALStore/Updated",
    "ALSystem/RobotNameChanged",
    "ALTabletService/error",
    "ALTabletService/message",
    "ALTabletService/onInputText",
    "ALTactileGesture/Gesture",
    "ALTactileGesture/Release",
    "ALTextToSpeech/CurrentBookMark",
    "ALTextToSpeech/CurrentSentence",
    "ALTextToSpeech/CurrentWord",
    "ALTextToSpeech/PositionOfCurrentWord",
    "ALTextToSpeech/Status",
    "ALTextToSpeech/TextDone",
    "ALTextToSpeech/TextInterrupted",
    "ALTextToSpeech/TextStarted",
    "ALTracker/CloseObjectDetected",
    "ALTracker/ColorBlobDetected",
    "ALVoiceEmotionAnalysis/EmotionRecognized",
    "ActiveDiagnosisErrorChanged",
    "AutonomousLife/CompletedActivity",
    "AutonomousLife/FocusedActivity",
    "AutonomousLife/LaunchSuggestions",
    "AutonomousLife/NextActivity",
    "AutonomousLife/State",
    "BackBumperPressed",
    "BacklightingDetection/BacklightingDetected",
    "BarcodeReader/BarcodeDetected",
    "BatteryChargeChanged",
    "BatteryChargingFlagChanged",
    "BatteryPowerPluggedChanged",
    "BatteryTrapIsOpen",
    "BehaviorsRun",
    "BodyStiffnessChanged",
    "ChestButtonPressed",
    "ClientConnected",
    "ClientDisconnected",
    "CloseObjectDetection/ObjectDetected",
    "CloseObjectDetection/ObjectNotDetected",
    "DarknessDetection/DarknessDetected",
    "Dialog/ActivateTopic",
    "Dialog/Answered",
    "Dialog/CannotMakeIt",
    "Dialog/CurrentString",
    "Dialog/DateCode",
    "Dialog/DeactivateTopic",
    "Dialog/Default",
    "Dialog/Failure",
    "Dialog/Fallback",
    "Dialog/Focus",
    "Dialog/FocusDescription",
    "Dialog/Hour",
    "Dialog/IsStarted",
    "Dialog/Language/English",
    "Dialog/Language/French",
    "Dialog/LastAnswer",
    "Dialog/LastInput",
    "Dialog/MainDialogStarted",
    "Dialog/MatchedApp",
    "Dialog/MatchedInput",
    "Dialog/MatchedLine",
    "Dialog/MatchedTopic",
    "Dialog/Minute",
    "Dialog/Morning",
    "Dialog/Night",
    "Dialog/NoOneSpeak",
    "Dialog/NoOneSpeak10",
    "Dialog/NoOneSpeak15",
    "Dialog/NoOneSpeak20",
    "Dialog/NoOneSpeak5",
    "Dialog/NotSpeaking10",
    "Dialog/NotSpeaking15",
    "Dialog/NotSpeaking20",
    "Dialog/NotSpeaking5",
    "Dialog/NotUnderstood",
    "Dialog/NotUnderstood2",
    "Dialog/NotUnderstood3",
    "Dialog/NotUnderstoodEvent",
    "Dialog/NothingToSay",
    "Dialog/OpenSession",
    "Dialog/RobotModel",
    "Dialog/RobotName",
    "Dialog/SaidMisunderstood",
    "Dialog/SameRule",
    "Dialog/Scope",
    "Dialog/Second",
    "Dialog/SpeakFailure",
    "Dialog/Tag",
    "Dialog/TalkTime",
    "Dialog/Year",
    "EngagementZones/FirstLimitDistanceUpdated",
    "EngagementZones/LimitAngleUpdated",
    "EngagementZones/MovementsInZonesUpdated",
    "EngagementZones/PeopleInZonesUpdated",
    "EngagementZones/PersonApproached",
    "EngagementZones/PersonEnteredZone1",
    "EngagementZones/PersonEnteredZone2",
    "EngagementZones/PersonEnteredZone3",
    "EngagementZones/PersonMovedAway",
    "EngagementZones/SecondLimitDistanceUpdated",
    "FaceCharacteristics/PersonSmiling",
    "FaceDetected",
    "FrontTactilTouched",
    "footContactChanged",
    "GazeAnalysis/PeopleLookingAtRobot",
    "GazeAnalysis/PersonStartsLookingAtRobot",
    "GazeAnalysis/PersonStopsLookingAtRobot",
    "HandLeftBackTouched",
    "HandLeftLeftTouched",
    "HandLeftRightTouched",
    "HandRightBackTouched",
    "HandRightLeftTouched",
    "HandRightRightTouched",
    "HotDeviceDetected",
    "LandmarkDetected",
    "LastWordRecognized",
    "Launchpad/BatteryIsCharging",
    "Launchpad/BatteryLevel",
    "Launchpad/BatteryStatus",
    "Launchpad/Date",
    "Launchpad/Day",
    "Launchpad/DayName",
    "Launchpad/DistanceOfTrackedHuman",
    "Launchpad/FocusCount/myPackageUUID/myActivityPath",
    "Launchpad/FocusedActivity",
    "Launchpad/HighestJoint",
    "Launchpad/HighestTemperature",
    "Launchpad/Hour",
    "Launchpad/LifeTime",
    "Launchpad/Lifted",
    "Launchpad/Minute",
    "Launchpad/MinuteOfDay",
    "Launchpad/Month",
    "Launchpad/MonthName",
    "Launchpad/NoMotionInZones",
    "Launchpad/NoPeopleInZones",
    "Launchpad/NumMotionZone1",
    "Launchpad/NumMotionZone2",
    "Launchpad/NumMotionZone3",
    "Launchpad/NumPeopleZone1",
    "Launchpad/NumPeopleZone2",
    "Launchpad/NumPeopleZone3",
    "Launchpad/PeopleNotSeen",
    "Launchpad/Posture",
    "Launchpad/PostureFamily",
    "Launchpad/PreviousActivity",
    "Launchpad/PreviousState",
    "Launchpad/RobotFellRecently",
    "Launchpad/RobotPushedRecently",
    "Launchpad/RobotType",
    "Launchpad/SameTrackedHuman",
    "Launchpad/State",
    "Launchpad/TemperatureStatus",
    "Launchpad/TrackedHumanIsLookingAtRobot",
    "Launchpad/WavingDetection",
    "Launchpad/Week",
    "Launchpad/Year",
    "Launchpad/ZoneOfTrackedHuman",
    "LeftBumperPressed",
    "MiddleTactilTouched",
    "MovementDetection/MovementDetected",
    "MovementDetection/NoMovement",
    "NAOqiReady",
    "Navigation/AvoidanceNavigator/AbsTargetModified",
    "Navigation/AvoidanceNavigator/MovingToFreeZone",
    "Navigation/AvoidanceNavigator/ObstacleDetected",
    "Navigation/AvoidanceNavigator/Status",
    "Navigation/AvoidanceNavigator/TrajectoryProgress",
    "Navigation/MotionDetected",
    "NetworkConnectStatus",
    "NetworkDefaultTechnologyChanged",
    "NetworkServiceAdded",
    "NetworkServiceInputRequired",
    "NetworkServiceRemoved",
    "NetworkServiceStateChanged",
    "NetworkStateChanged",
    "NetworkTechnologyAdded",
    "NetworkTechnologyRemoved",
    "notificationAdded",
    "notificationRemoved",
    "PassiveDiagnosisErrorChanged",
    "PeoplePerception/JustArrived",
    "PeoplePerception/JustLeft",
    "PeoplePerception/MaximumDetectionRangeUpdated",
    "PeoplePerception/NonVisiblePeopleList",
    "PeoplePerception/PeopleDetected",
    "PeoplePerception/PeopleList",
    "PeoplePerception/PopulationUpdated",
    "PeoplePerception/VisiblePeopleList",
    "PictureDetected",
    "PostureChanged",
    "PostureFamilyChanged",
    "preferenceAdded",
    "preferenceChanged",
    "preferenceDomainRemoved",
    "preferenceRemoved",
    "preferenceSynchronized",
    "RearTactilTouched",
    "RightBumperPressed",
    "redBallDetected",
    "robotHasFallen",
    "robotIsWakeUp",
    "SittingPeopleDetection/PersonSittingDown",
    "SittingPeopleDetection/PersonStandingUp",
    "SonarLeftDetected",
    "SonarLeftNothingDetected",
    "SonarRightDetected",
    "SonarRightNothingDetected",
    "SoundDetected",
    "SpeechDetected",
    "TemperatureStatusChanged",
    "TouchChanged",
    "UserSession/CreatedUsers",
    "UserSession/DeletedUsers",
    "UserSession/FocusedUser",
    "UserSession/NoOpenSessions",
    "UserSession/SessionsClosed",
    "UserSession/SessionsOpened",
    "UserSession/ShouldExitInteractiveActivity",
    "VisualCompass/Deviation",
    "VisualCompass/FinalDeviation",
    "VisualCompass/InvalidReference",
    "VisualCompass/Match",
    "VisualCompass/MoveAbort",
    "VisualCompass/NewReferenceImageSet",
    "WavingDetection/PersonWaving",
    "WavingDetection/PersonWavingCenter",
    "WavingDetection/PersonWavingLeft",
    "WavingDetection/PersonWavingRight",
    "WavingDetection/Waving",
    "WordRecognized",
    "WordRecognizedAndGrammar"
}


class NaoQiNames(str, Enum):
    ALAnimatedSpeech_EndOfAnimatedSpeech = "ALAnimatedSpeech/EndOfAnimatedSpeech"
    ALAudioSourceLocalization_SoundLocated = "ALAudioSourceLocalization/SoundLocated"
    ALAudioSourceLocalization_SoundsLocated = "ALAudioSourceLocalization/SoundsLocated"
    ALBasicAwareness_HumanLost = "ALBasicAwareness/HumanLost"
    ALBasicAwareness_HumanTracked = "ALBasicAwareness/HumanTracked"
    ALBasicAwareness_StimulusDetected = "ALBasicAwareness/StimulusDetected"
    ALBattery_ConnectedToChargingStation = "ALBattery/ConnectedToChargingStation"
    ALBehaviorManager_BehaviorAdded = "ALBehaviorManager/BehaviorAdded"
    ALBehaviorManager_BehaviorRemoved = "ALBehaviorManager/BehaviorRemoved"
    ALBehaviorManager_BehaviorUpdated = "ALBehaviorManager/BehaviorUpdated"
    ALBehaviorManager_BehaviorsAdded = "ALBehaviorManager/BehaviorsAdded"
    ALChestButton_DoubleClickOccurred = "ALChestButton/DoubleClickOccurred"
    ALChestButton_SimpleClickOccurred = "ALChestButton/SimpleClickOccurred"
    ALChestButton_TripleClickOccurred = "ALChestButton/TripleClickOccurred"
    ALDiagnosis_RobotIsReady = "ALDiagnosis/RobotIsReady"
    ALLocalization_FullScanBegin = "ALLocalization/FullScanBegin"
    ALLocalization_FullScanInsufficient = "ALLocalization/FullScanInsufficient"
    ALLocalization_FullScanSuccess = "ALLocalization/FullScanSuccess"
    ALLocalization_GoToBegin = "ALLocalization/GoToBegin"
    ALLocalization_GoToFailed = "ALLocalization/GoToFailed"
    ALLocalization_GoToLost = "ALLocalization/GoToLost"
    ALLocalization_GoToNextMove = "ALLocalization/GoToNextMove"
    ALLocalization_GoToSuccess = "ALLocalization/GoToSuccess"
    ALLocalization_HalfScanBegin = "ALLocalization/HalfScanBegin"
    ALLocalization_HalfScanInsufficient = "ALLocalization/HalfScanInsufficient"
    ALLocalization_HalfScanSuccess = "ALLocalization/HalfScanSuccess"
    ALLocalization_LocalizeBegin = "ALLocalization/LocalizeBegin"
    ALLocalization_LocalizeDirectionBegin = "ALLocalization/LocalizeDirectionBegin"
    ALLocalization_LocalizeDirectionLost = "ALLocalization/LocalizeDirectionLost"
    ALLocalization_LocalizeDirectionSuccess = "ALLocalization/LocalizeDirectionSuccess"
    ALLocalization_LocalizeLost = "ALLocalization/LocalizeLost"
    ALLocalization_LocalizeSuccess = "ALLocalization/LocalizeSuccess"
    ALLocalization_OdometryBegin = "ALLocalization/OdometryBegin"
    ALLocalization_OdometryInsufficient = "ALLocalization/OdometryInsufficient"
    ALLocalization_StartingComputation = "ALLocalization/StartingComputation"
    ALLocalization_StoppingComputation = "ALLocalization/StoppingComputation"
    ALLocalization_UTurnEnd = "ALLocalization/UTurnEnd"
    ALMemory_KeyAdded = "ALMemory/KeyAdded"
    ALMemory_KeyRemoved = "ALMemory/KeyRemoved"
    ALMemory_KeyTypeChanged = "ALMemory/KeyTypeChanged"
    ALMotion_MoveFailed = "ALMotion/MoveFailed"
    ALMotion_Protection_DisabledDevicesChanged = "ALMotion/Protection/DisabledDevicesChanged"
    ALMotion_RobotIsFalling = "ALMotion/RobotIsFalling"
    ALMotion_RobotIsStand = "ALMotion/RobotIsStand"
    ALMotion_RobotPushed = "ALMotion/RobotPushed"
    ALMotion_Safety_ChainVelocityClipped = "ALMotion/Safety/ChainVelocityClipped"
    ALMotion_Safety_PushRecovery = "ALMotion/Safety/PushRecovery"
    ALMotion_Stiffness_wakeUpFinished = "ALMotion/Stiffness/wakeUpFinished"
    ALMotion_Stiffness_wakeUpStarted = "ALMotion/Stiffness/wakeUpStarted"
    ALRecharge_CloseToChargingStation = "ALRecharge/CloseToChargingStation"
    ALRecharge_ConnectedToChargingStation = "ALRecharge/ConnectedToChargingStation"
    ALRecharge_DockingBackwardStarted = "ALRecharge/DockingBackwardStarted"
    ALRecharge_DockingFailed = "ALRecharge/DockingFailed"
    ALRecharge_DockingSuccess = "ALRecharge/DockingSuccess"
    ALRecharge_DockingUTurnStarted = "ALRecharge/DockingUTurnStarted"
    ALRecharge_LeaveFailed = "ALRecharge/LeaveFailed"
    ALRecharge_LeaveSuccess = "ALRecharge/LeaveSuccess"
    ALRecharge_MoveFailed = "ALRecharge/MoveFailed"
    ALRecharge_SearchStopped = "ALRecharge/SearchStopped"
    ALRecharge_StationDetected = "ALRecharge/StationDetected"
    ALRecharge_StationNotFound = "ALRecharge/StationNotFound"
    ALRecharge_StatusChanged = "ALRecharge/StatusChanged"
    ALSoundLocalization_SoundLocated = "ALSoundLocalization/SoundLocated"
    ALSoundLocalization_SoundsLocated = "ALSoundLocalization/SoundsLocated"
    ALSpeechRecognition_ActiveListening = "ALSpeechRecognition/ActiveListening"
    ALSpeechRecognition_IsRunning = "ALSpeechRecognition/IsRunning"
    ALSpeechRecognition_Status = "ALSpeechRecognition/Status"
    ALStore_SystemImageDownloaded = "ALStore/SystemImageDownloaded"
    ALStore_Updated = "ALStore/Updated"
    ALSystem_RobotNameChanged = "ALSystem/RobotNameChanged"
    ALTabletService_error = "ALTabletService/error"
    ALTabletService_message = "ALTabletService/message"
    ALTabletService_onInputText = "ALTabletService/onInputText"
    ALTactileGesture_Gesture = "ALTactileGesture/Gesture"
    ALTactileGesture_Release = "ALTactileGesture/Release"
    ALTextToSpeech_CurrentBookMark = "ALTextToSpeech/CurrentBookMark"
    ALTextToSpeech_CurrentSentence = "ALTextToSpeech/CurrentSentence"
    ALTextToSpeech_CurrentWord = "ALTextToSpeech/CurrentWord"
    ALTextToSpeech_PositionOfCurrentWord = "ALTextToSpeech/PositionOfCurrentWord"
    ALTextToSpeech_Status = "ALTextToSpeech/Status"
    ALTextToSpeech_TextDone = "ALTextToSpeech/TextDone"
    ALTextToSpeech_TextInterrupted = "ALTextToSpeech/TextInterrupted"
    ALTextToSpeech_TextStarted = "ALTextToSpeech/TextStarted"
    ALTracker_CloseObjectDetected = "ALTracker/CloseObjectDetected"
    ALTracker_ColorBlobDetected = "ALTracker/ColorBlobDetected"
    ALVoiceEmotionAnalysis_EmotionRecognized = "ALVoiceEmotionAnalysis/EmotionRecognized"
    ActiveDiagnosisErrorChanged = "ActiveDiagnosisErrorChanged"
    AutonomousLife_CompletedActivity = "AutonomousLife/CompletedActivity"
    AutonomousLife_FocusedActivity = "AutonomousLife/FocusedActivity"
    AutonomousLife_LaunchSuggestions = "AutonomousLife/LaunchSuggestions"
    AutonomousLife_NextActivity = "AutonomousLife/NextActivity"
    AutonomousLife_State = "AutonomousLife/State"
    BackBumperPressed = "BackBumperPressed"
    BacklightingDetection_BacklightingDetected = "BacklightingDetection/BacklightingDetected"
    BarcodeReader_BarcodeDetected = "BarcodeReader/BarcodeDetected"
    BatteryChargeChanged = "BatteryChargeChanged"
    BatteryChargingFlagChanged = "BatteryChargingFlagChanged"
    BatteryPowerPluggedChanged = "BatteryPowerPluggedChanged"
    BatteryTrapIsOpen = "BatteryTrapIsOpen"
    BehaviorsRun = "BehaviorsRun"
    BodyStiffnessChanged = "BodyStiffnessChanged"
    ChestButtonPressed = "ChestButtonPressed"
    ClientConnected = "ClientConnected"
    ClientDisconnected = "ClientDisconnected"
    CloseObjectDetection_ObjectDetected = "CloseObjectDetection/ObjectDetected"
    CloseObjectDetection_ObjectNotDetected = "CloseObjectDetection/ObjectNotDetected"
    DarknessDetection_DarknessDetected = "DarknessDetection/DarknessDetected"
    Dialog_ActivateTopic = "Dialog/ActivateTopic"
    Dialog_Answered = "Dialog/Answered"
    Dialog_CannotMakeIt = "Dialog/CannotMakeIt"
    Dialog_CurrentString = "Dialog/CurrentString"
    Dialog_DateCode = "Dialog/DateCode"
    Dialog_DeactivateTopic = "Dialog/DeactivateTopic"
    Dialog_Default = "Dialog/Default"
    Dialog_Failure = "Dialog/Failure"
    Dialog_Fallback = "Dialog/Fallback"
    Dialog_Focus = "Dialog/Focus"
    Dialog_FocusDescription = "Dialog/FocusDescription"
    Dialog_Hour = "Dialog/Hour"
    Dialog_IsStarted = "Dialog/IsStarted"
    Dialog_Language_English = "Dialog/Language/English"
    Dialog_Language_French = "Dialog/Language/French"
    Dialog_LastAnswer = "Dialog/LastAnswer"
    Dialog_LastInput = "Dialog/LastInput"
    Dialog_MainDialogStarted = "Dialog/MainDialogStarted"
    Dialog_MatchedApp = "Dialog/MatchedApp"
    Dialog_MatchedInput = "Dialog/MatchedInput"
    Dialog_MatchedLine = "Dialog/MatchedLine"
    Dialog_MatchedTopic = "Dialog/MatchedTopic"
    Dialog_Minute = "Dialog/Minute"
    Dialog_Morning = "Dialog/Morning"
    Dialog_Night = "Dialog/Night"
    Dialog_NoOneSpeak = "Dialog/NoOneSpeak"
    Dialog_NoOneSpeak10 = ""
    Dialog_NoOneSpeak15 = ""
    Dialog_NoOneSpeak20 = ""
    Dialog_NoOneSpeak5 = ""
    Dialog_NotSpeaking10 = ""
    Dialog_NotSpeaking15 = ""
    Dialog_NotSpeaking20 = ""
    Dialog_NotSpeaking5 = ""
    Dialog_NotUnderstood = "Dialog/NotUnderstood"
    Dialog_NotUnderstood2 = ""
    Dialog_NotUnderstood3 = ""
    Dialog_NotUnderstoodEvent = "Dialog/NotUnderstoodEvent"
    Dialog_NothingToSay = "Dialog/NothingToSay"
    Dialog_OpenSession = "Dialog/OpenSession"
    Dialog_RobotModel = "Dialog/RobotModel"
    Dialog_RobotName = "Dialog/RobotName"
    Dialog_SaidMisunderstood = "Dialog/SaidMisunderstood"
    Dialog_SameRule = "Dialog/SameRule"
    Dialog_Scope = "Dialog/Scope"
    Dialog_Second = "Dialog/Second"
    Dialog_SpeakFailure = "Dialog/SpeakFailure"
    Dialog_Tag = "Dialog/Tag"
    Dialog_TalkTime = "Dialog/TalkTime"
    Dialog_Year = "Dialog/Year"
    EngagementZones_FirstLimitDistanceUpdated = "EngagementZones/FirstLimitDistanceUpdated"
    EngagementZones_LimitAngleUpdated = "EngagementZones/LimitAngleUpdated"
    EngagementZones_MovementsInZonesUpdated = "EngagementZones/MovementsInZonesUpdated"
    EngagementZones_PeopleInZonesUpdated = "EngagementZones/PeopleInZonesUpdated"
    EngagementZones_PersonApproached = "EngagementZones/PersonApproached"
    EngagementZones_PersonEnteredZone1 = ""
    EngagementZones_PersonEnteredZone2 = ""
    EngagementZones_PersonEnteredZone3 = ""
    EngagementZones_PersonMovedAway = "EngagementZones/PersonMovedAway"
    EngagementZones_SecondLimitDistanceUpdated = "EngagementZones/SecondLimitDistanceUpdated"
    FaceCharacteristics_PersonSmiling = "FaceCharacteristics/PersonSmiling"
    FaceDetected = "FaceDetected"
    FrontTactilTouched = "FrontTactilTouched"
    footContactChanged = "footContactChanged"
    GazeAnalysis_PeopleLookingAtRobot = "GazeAnalysis/PeopleLookingAtRobot"
    GazeAnalysis_PersonStartsLookingAtRobot = "GazeAnalysis/PersonStartsLookingAtRobot"
    GazeAnalysis_PersonStopsLookingAtRobot = "GazeAnalysis/PersonStopsLookingAtRobot"
    HandLeftBackTouched = "HandLeftBackTouched"
    HandLeftLeftTouched = "HandLeftLeftTouched"
    HandLeftRightTouched = "HandLeftRightTouched"
    HandRightBackTouched = "HandRightBackTouched"
    HandRightLeftTouched = "HandRightLeftTouched"
    HandRightRightTouched = "HandRightRightTouched"
    HotDeviceDetected = "HotDeviceDetected"
    LandmarkDetected = "LandmarkDetected"
    LastWordRecognized = "LastWordRecognized"
    Launchpad_BatteryIsCharging = "Launchpad/BatteryIsCharging"
    Launchpad_BatteryLevel = "Launchpad/BatteryLevel"
    Launchpad_BatteryStatus = "Launchpad/BatteryStatus"
    Launchpad_Date = "Launchpad/Date"
    Launchpad_Day = "Launchpad/Day"
    Launchpad_DayName = "Launchpad/DayName"
    Launchpad_DistanceOfTrackedHuman = "Launchpad/DistanceOfTrackedHuman"
    Launchpad_FocusCount_myPackageUUID_myActivityPath = "Launchpad/FocusCount/myPackageUUID/myActivityPath"
    Launchpad_FocusedActivity = "Launchpad/FocusedActivity"
    Launchpad_HighestJoint = "Launchpad/HighestJoint"
    Launchpad_HighestTemperature = "Launchpad/HighestTemperature"
    Launchpad_Hour = "Launchpad/Hour"
    Launchpad_LifeTime = "Launchpad/LifeTime"
    Launchpad_Lifted = "Launchpad/Lifted"
    Launchpad_Minute = "Launchpad/Minute"
    Launchpad_MinuteOfDay = "Launchpad/MinuteOfDay"
    Launchpad_Month = "Launchpad/Month"
    Launchpad_MonthName = "Launchpad/MonthName"
    Launchpad_NoMotionInZones = "Launchpad/NoMotionInZones"
    Launchpad_NoPeopleInZones = "Launchpad/NoPeopleInZones"
    Launchpad_NumMotionZone1 = ""
    Launchpad_NumMotionZone2 = ""
    Launchpad_NumMotionZone3 = ""
    Launchpad_NumPeopleZone1 = ""
    Launchpad_NumPeopleZone2 = ""
    Launchpad_NumPeopleZone3 = ""
    Launchpad_PeopleNotSeen = "Launchpad/PeopleNotSeen"
    Launchpad_Posture = "Launchpad/Posture"
    Launchpad_PostureFamily = "Launchpad/PostureFamily"
    Launchpad_PreviousActivity = "Launchpad/PreviousActivity"
    Launchpad_PreviousState = "Launchpad/PreviousState"
    Launchpad_RobotFellRecently = "Launchpad/RobotFellRecently"
    Launchpad_RobotPushedRecently = "Launchpad/RobotPushedRecently"
    Launchpad_RobotType = "Launchpad/RobotType"
    Launchpad_SameTrackedHuman = "Launchpad/SameTrackedHuman"
    Launchpad_State = "Launchpad/State"
    Launchpad_TemperatureStatus = "Launchpad/TemperatureStatus"
    Launchpad_TrackedHumanIsLookingAtRobot = "Launchpad/TrackedHumanIsLookingAtRobot"
    Launchpad_WavingDetection = "Launchpad/WavingDetection"
    Launchpad_Week = "Launchpad/Week"
    Launchpad_Year = "Launchpad/Year"
    Launchpad_ZoneOfTrackedHuman = "Launchpad/ZoneOfTrackedHuman"
    LeftBumperPressed = "LeftBumperPressed"
    MiddleTactilTouched = "MiddleTactilTouched"
    MovementDetection_MovementDetected = "MovementDetection/MovementDetected"
    MovementDetection_NoMovement = "MovementDetection/NoMovement"
    NAOqiReady = "NAOqiReady"
    Navigation_AvoidanceNavigator_AbsTargetModified = "Navigation/AvoidanceNavigator/AbsTargetModified"
    Navigation_AvoidanceNavigator_MovingToFreeZone = "Navigation/AvoidanceNavigator/MovingToFreeZone"
    Navigation_AvoidanceNavigator_ObstacleDetected = "Navigation/AvoidanceNavigator/ObstacleDetected"
    Navigation_AvoidanceNavigator_Status = "Navigation/AvoidanceNavigator/Status"
    Navigation_AvoidanceNavigator_TrajectoryProgress = "Navigation/AvoidanceNavigator/TrajectoryProgress"
    Navigation_MotionDetected = "Navigation/MotionDetected"
    NetworkConnectStatus = "NetworkConnectStatus"
    NetworkDefaultTechnologyChanged = "NetworkDefaultTechnologyChanged"
    NetworkServiceAdded = "NetworkServiceAdded"
    NetworkServiceInputRequired = "NetworkServiceInputRequired"
    NetworkServiceRemoved = "NetworkServiceRemoved"
    NetworkServiceStateChanged = "NetworkServiceStateChanged"
    NetworkStateChanged = "NetworkStateChanged"
    NetworkTechnologyAdded = "NetworkTechnologyAdded"
    NetworkTechnologyRemoved = "NetworkTechnologyRemoved"
    notificationAdded = "notificationAdded"
    notificationRemoved = "notificationRemoved"
    PassiveDiagnosisErrorChanged = "PassiveDiagnosisErrorChanged"
    PeoplePerception_JustArrived = "PeoplePerception/JustArrived"
    PeoplePerception_JustLeft = "PeoplePerception/JustLeft"
    PeoplePerception_MaximumDetectionRangeUpdated = "PeoplePerception/MaximumDetectionRangeUpdated"
    PeoplePerception_NonVisiblePeopleList = "PeoplePerception/NonVisiblePeopleList"
    PeoplePerception_PeopleDetected = "PeoplePerception/PeopleDetected"
    PeoplePerception_PeopleList = "PeoplePerception/PeopleList"
    PeoplePerception_PopulationUpdated = "PeoplePerception/PopulationUpdated"
    PeoplePerception_VisiblePeopleList = "PeoplePerception/VisiblePeopleList"
    PictureDetected = "PictureDetected"
    PostureChanged = "PostureChanged"
    PostureFamilyChanged = "PostureFamilyChanged"
    preferenceAdded = "preferenceAdded"
    preferenceChanged = "preferenceChanged"
    preferenceDomainRemoved = "preferenceDomainRemoved"
    preferenceRemoved = "preferenceRemoved"
    preferenceSynchronized = "preferenceSynchronized"
    RearTactilTouched = "RearTactilTouched"
    RightBumperPressed = "RightBumperPressed"
    redBallDetected = "redBallDetected"
    robotHasFallen = "robotHasFallen"
    robotIsWakeUp = "robotIsWakeUp"
    SittingPeopleDetection_PersonSittingDown = "SittingPeopleDetection/PersonSittingDown"
    SittingPeopleDetection_PersonStandingUp = "SittingPeopleDetection/PersonStandingUp"
    SonarLeftDetected = "SonarLeftDetected"
    SonarLeftNothingDetected = "SonarLeftNothingDetected"
    SonarRightDetected = "SonarRightDetected"
    SonarRightNothingDetected = "SonarRightNothingDetected"
    SoundDetected = "SoundDetected"
    SpeechDetected = "SpeechDetected"
    TemperatureStatusChanged = "TemperatureStatusChanged"
    TouchChanged = "TouchChanged"
    UserSession_CreatedUsers = "UserSession/CreatedUsers"
    UserSession_DeletedUsers = "UserSession/DeletedUsers"
    UserSession_FocusedUser = "UserSession/FocusedUser"
    UserSession_NoOpenSessions = "UserSession/NoOpenSessions"
    UserSession_SessionsClosed = "UserSession/SessionsClosed"
    UserSession_SessionsOpened = "UserSession/SessionsOpened"
    UserSession_ShouldExitInteractiveActivity = "UserSession/ShouldExitInteractiveActivity"
    VisualCompass_Deviation = "VisualCompass/Deviation"
    VisualCompass_FinalDeviation = "VisualCompass/FinalDeviation"
    VisualCompass_InvalidReference = "VisualCompass/InvalidReference"
    VisualCompass_Match = "VisualCompass/Match"
    VisualCompass_MoveAbort = "VisualCompass/MoveAbort"
    VisualCompass_NewReferenceImageSet = "VisualCompass/NewReferenceImageSet"
    WavingDetection_PersonWaving = "WavingDetection/PersonWaving"
    WavingDetection_PersonWavingCenter = "WavingDetection/PersonWavingCenter"
    WavingDetection_PersonWavingLeft = "WavingDetection/PersonWavingLeft"
    WavingDetection_PersonWavingRight = "WavingDetection/PersonWavingRight"
    WavingDetection_Waving = "WavingDetection/Waving"
    WordRecognized = "WordRecognized"
    WordRecognizedAndGrammar = "WordRecognizedAndGrammar"