#!/usr/bin/env python

"""The setup script."""

from setuptools import setup, find_packages

with open('README.md') as readme_file:
    readme = readme_file.read()

with open('CHANGELOG.md') as history_file:
    history = history_file.read()

requirements = ["aaambos @ git+https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos@main"]

test_requirements = [ ]

setup(
    author="Florian Schröder",
    author_email='florian.schroeder@uni-bielefeld.de',
    python_requires='>=3.10',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.10',
    ],
    description="Embodiment module for naoqi robots (pepper, nao) that uses the naoqi SDK.",
    install_requires=requirements,
    license="MIT license",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords=['aaambos', 'naoqi_robot_embodiment'],
    name='naoqi_robot_embodiment',
    packages=find_packages(include=['naoqi_robot_embodiment', 'naoqi_robot_embodiment.*']),
    test_suite='tests',
    tests_require=test_requirements,
    url='https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos_pkg_naoqi_robot_embodiment',
    version='0.1.0',
    zip_safe=False,
)